package com.jgeek.fastpass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastpassConsoleApplication {

	public static void main(String[] args) {
		SpringApplication.run(FastpassConsoleApplication.class, args);
	}

}
